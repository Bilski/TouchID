//
//  ListInteractorImpl.swift
//  TouchID
//
//  Created by Tomasz Bilski on 09.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

class ListInteractorImpl: NSObject, ListInteractorInput {
    weak var output: ListInteractorOutput?
    let dataManager: ListDataManager
    let authenticationManager: AuthenticationManager
    
    init(dataManager: ListDataManager,
         authenticationManager: AuthenticationManager) {
        self.dataManager = dataManager
        self.authenticationManager = authenticationManager
    }

    func findNoteItems() {
        dataManager.getAllNotes { notes in
            let noteItems = self.mapToNoteItemsFromNotes(notes)
            self.output?.foundNoteItems(noteItems: noteItems)
        }
    }
    
    func saveNewEntryWithDateAndPlaceholder(date: Date, text: String) {
        let newEntry = Note(date: date, text: text)
        dataManager.addNewEntry(entry: newEntry)
    }
    
    func logUserOut() {
        authenticationManager.logUserOut()
    }
    
    func checkIfLoggedIn() -> Bool {
        return authenticationManager.checkIfLoggedIn()
    }
    
    private func mapToNoteItemsFromNotes(_ notes: [Note]) -> [NoteItem] {
        /* mapowanie z entity; Interactor nigdy nie powinien przekazywac
        encji do wartstwy prezentacji (do Presentera) */
        
        var noteItems: [NoteItem] = []
        
        for note in notes {
            let noteItem = NoteItem(date: note.date, text: note.text)
            noteItems.append(noteItem)
        }
        return noteItems
    }
}

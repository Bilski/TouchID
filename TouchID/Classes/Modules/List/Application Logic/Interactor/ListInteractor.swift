//
//  ListInteractor.swift
//  TouchID
//
//  Created by Tomasz Bilski on 09.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

protocol ListInteractorInput {
    func findNoteItems()
    func saveNewEntryWithDateAndPlaceholder(date: Date, text: String)
    func logUserOut()
    func checkIfLoggedIn() -> Bool
}

protocol ListInteractorOutput: AnyObject {
    func foundNoteItems(noteItems: [NoteItem])
}

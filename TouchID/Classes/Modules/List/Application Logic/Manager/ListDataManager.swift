//
//  ListDataManager.swift
//  TouchID
//
//  Created by Tomasz Bilski on 09.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

class ListDataManager: NSObject {
    var dataStore: CoreDataStore
    
    init(dataStore: CoreDataStore) {
        self.dataStore = dataStore
        super.init()
    }
    
    func addNewEntry(entry: Note) {
        let newEntry = dataStore.newNote() as NoteCDModel
        newEntry.date = entry.date as NSDate
        newEntry.noteText = entry.text
        
        dataStore.save()
    }
    
    func getAllNotes(completion: @escaping (([Note]) -> Void)) {
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        
        dataStore.fetchEntries(sortDescriptors: [sortDescriptor]) { entries in
            let notes = self.mapToNotesFromDataStoreEntries(entries)
            completion(notes)
        }
    }
    
    private func mapToNotesFromDataStoreEntries(_ entries: [NoteCDModel]) -> [Note] {
        var notes: [Note] = []
        
        for managedNote in entries {
            if let date = managedNote.date,
                let text = managedNote.noteText {
                let note = Note(date: date as Date, text: text)
                notes.append(note)
            }
        }
        return notes
    }
}

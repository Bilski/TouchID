//
//  ListRouting.swift
//  TouchID
//
//  Created by Tomasz Bilski on 10.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation
import UIKit

let ListViewControllerIdentifier = "ListViewController"

class ListRouting: NSObject {
    var listViewController: ListViewController?
    var rootRouting: RootRouting?
    var listPresenter: ListPresenter?
    var loginRouting: LoginRouting?
    
    func presentListInterfaceFromWindow(window: UIWindow) {
        let viewController = listViewControllerFromStoryboard()
        viewController.eventHandler = listPresenter
        listViewController = viewController
        listPresenter!.userInterface = viewController
        rootRouting?.showRootViewController(viewController: viewController, inWindow: window)
    }
    
    func presentLoginInterface() {
        loginRouting?.presentLoginInterfaceFromViewController(viewController: listViewController!)
    }
    
    func listViewControllerFromStoryboard() -> ListViewController {
        let storyboard = mainStoryboard()
        let viewController = storyboard.instantiateViewController(withIdentifier: ListViewControllerIdentifier) as! ListViewController
        return viewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard
    }
}

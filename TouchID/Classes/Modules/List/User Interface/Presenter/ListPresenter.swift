//
//  ListPresenter.swift
//  TouchID
//
//  Created by Tomasz Bilski on 10.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

class ListPresenter: NSObject, ListInteractorOutput, ListModuleInterface, LoginModuleDelegate {
    weak var userInterface: ListViewInterface?
    var listInteractor: ListInteractorInput!
    var listRouting: ListRouting?
    
    func foundNoteItems(noteItems: [NoteItem]) {
        userInterface?.showItems(data: noteItems)
    }
    
    func addNewEntry() {
        listInteractor.saveNewEntryWithDateAndPlaceholder(date: Date(), text: "New note")
        updateView()
    }
    
    func updateView() {
        listInteractor?.findNoteItems()
    }
    
    func showLoginView() {
        if !listInteractor.checkIfLoggedIn() {
            listRouting?.presentLoginInterface()
        }
    }
    
    func logUserOut() {
        listInteractor.logUserOut()
    }
    
    func loginModuleAuthenticationSucceeded() {
        userInterface?.showAllViews()
    }
}

//
//  ListViewInterface.swift
//  TouchID
//
//  Created by Tomasz Bilski on 10.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

protocol ListViewInterface: AnyObject {
    func showItems(data: [NoteItem])
    func showAllViews()
}

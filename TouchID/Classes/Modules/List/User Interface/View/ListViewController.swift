//
//  ListViewController.swift
//  TouchID
//
//  Created by Tomasz Bilski on 10.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation
import UIKit

let ListEntryCellIdentifier = "ListEntryCell"

class ListViewController: UIViewController, ListViewInterface, UITableViewDelegate, UITableViewDataSource {
    var eventHandler: ListModuleInterface?
    var dataProperty: [NoteItem]?
    var didReturnFromBackground = false
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(ListViewController.appWillResignActive(_:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(ListViewController.appDidBecomeActive(_:)), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        configureView()
        hideAllViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        eventHandler?.updateView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        eventHandler?.showLoginView()
    }
    
    private func configureView() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapAddButton(_:)))
        navigationItem.rightBarButtonItem = addButton
    }
    
    @objc func didTapAddButton(_ sender: Any) {
        eventHandler?.addNewEntry()
    }
    @IBAction func logout(_ sender: Any) {
        eventHandler?.logUserOut()
        eventHandler?.showLoginView()
    }
    
    func showItems(data: [NoteItem]) {
        dataProperty = data
        reloadEntries()
    }
    
    func showAllViews() {
        view.alpha = 1
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func hideAllViews() {
        view.alpha = 0
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func reloadEntries() {
        tableView.reloadData()
    }
    
    // MARK: - NSNotification: przejscie do backgroundu i powrot do foregroundu
    
    @objc func appWillResignActive(_ notification : Notification) {
        hideAllViews()
        eventHandler?.logUserOut()
        didReturnFromBackground = true
    }
    
    @objc func appDidBecomeActive(_ notification : Notification) {
        if didReturnFromBackground {
            eventHandler?.showLoginView()
            showAllViews()
        }
    }
    
    // MARK: - UITableViewDelegate and UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProperty?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = dataProperty![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ListEntryCellIdentifier, for: indexPath)
        
        cell.textLabel?.text = item.text
        
        return cell
    }
}

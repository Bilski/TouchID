//
//  NoteItem.swift
//  TouchID
//
//  Created by Tomasz Bilski on 09.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

struct NoteItem {
    let date: Date
    let text: String
    
    init(date: Date, text: String) {
        self.date = date
        self.text = text
    }
}

//
//  LoginPresenter.swift
//  TouchID
//
//  Created by Tomasz Bilski on 11.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

class LoginPresenter: NSObject, LoginModuleInterface, LoginInteractorOutput {
    weak var userInterface: LoginViewInterface?
    weak var loginModuleDelegate: LoginModuleDelegate?
    var loginInteractor: LoginInteractorInput!
    var loginRouting: LoginRouting?
    
    func adjustLoginViews() {
        let hasLogin = loginInteractor.checkIfCredentialsExist()
        let isFaceIDAvailable = loginInteractor.isFaceIDAvailable()
        
        if hasLogin {
            userInterface?.setLoginButtonTitle(text: "Login", for: .normal)
            userInterface?.setLoginButtonTag(.login)
        } else {
            userInterface?.setLoginButtonTitle(text: "Create", for: .normal)
            userInterface?.setLoginButtonTag(.create)
        }
        
        if let storedUsername = loginInteractor?.getUsername() {
            userInterface?.setUsernameFieldWithText(storedUsername)
        }
        
        if !loginInteractor.canEvaluatePolicy() {
            userInterface?.hideTouchIDButton()
        }
        
        if isFaceIDAvailable {
            userInterface?.setImageOnTouchIDButton(imageName: "FaceIcon", for: .normal)
        } else {
            userInterface?.setImageOnTouchIDButton(imageName: "TouchIcon", for: .normal)
        }
    }
    
    func startBioAuthentication() {
        if loginInteractor.canEvaluatePolicy() {
            loginInteractor.startBioAuthentication()
        }
    }
    
    func displayAlertViewForIncorrectCredentials() {
        loginRouting?.presentAlertView(message: "Wrong username or password", buttonTitle: "Spróbuj ponownie")
    }
    
    func authenticationFailed(message: String) {
        loginRouting?.presentAlertView(message: message, buttonTitle: "Ups!")
    }
    
    func authenticationSucceded() {
        makeAdjustmentsAfterUserLoggedIn()
    }
    
    func createCredentials(username: String, password: String) {
        let hasLogin = loginInteractor.checkIfCredentialsExist()
        if !hasLogin && !username.isEmpty {
            loginInteractor.storeUsername(username)
        }
        loginInteractor.savePassword(username: username, password: password)
        loginInteractor.setCredentialsExist()
        userInterface?.setLoginButtonTag(.login)
        makeAdjustmentsAfterUserLoggedIn()
    }
    
    func checkCredentials(username: String, password: String) {
        guard loginInteractor.getUsername() == username else {
            displayAlertViewForIncorrectCredentials()
            return
        }
        if loginInteractor.checkIfPasswordIsCorrect(username: username,
                                                    password: password) {
            makeAdjustmentsAfterUserLoggedIn()
        } else {
            displayAlertViewForIncorrectCredentials()
        }
    }
    
    private func makeAdjustmentsAfterUserLoggedIn() {
        loginInteractor.logUserIn()
        loginRouting?.dismissLoginInterface()
        loginModuleDelegate?.loginModuleAuthenticationSucceeded()
    }
}

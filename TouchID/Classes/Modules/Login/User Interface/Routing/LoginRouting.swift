//
//  LoginRouting.swift
//  TouchID
//
//  Created by Tomasz Bilski on 11.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation
import UIKit

let LoginViewControllerIdentifier = "LoginViewController"

class LoginRouting: NSObject {
    var loginPresenter: LoginPresenter?
    var presentedViewController: UIViewController?
    
    func presentLoginInterfaceFromViewController(viewController: UIViewController) {
        guard presentedViewController == nil else {
            return
        }
        let newViewController = loginViewController()
        newViewController.modalPresentationStyle = .fullScreen
        newViewController.eventHandler = loginPresenter
        loginPresenter!.userInterface = newViewController
        viewController.present(newViewController, animated: true, completion: nil)
        presentedViewController = newViewController
    }
    
    func loginViewController() -> LoginViewController {
        let storyboard = mainStoryboard()
        let loginViewController: LoginViewController = storyboard.instantiateViewController(withIdentifier: LoginViewControllerIdentifier) as! LoginViewController
        return loginViewController
    }
    
    func dismissLoginInterface() {
        presentedViewController?.dismiss(animated: true, completion: nil)
        presentedViewController = nil
    }
    
    func presentAlertView(message: String, buttonTitle: String) {
        let alertView = UIAlertController(title: "Login problem" , message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: buttonTitle, style: .default)
        alertView.addAction(okAction)
        presentedViewController?.present(alertView, animated: true)
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard
    }
}

//
//  LoginViewInterface.swift
//  TouchID
//
//  Created by Tomasz Bilski on 11.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewInterface: AnyObject {
    func setLoginButtonTitle(text: String, for state: UIControlState)
    func setLoginButtonTag(_ tag: LoginButtonTag)
    func setUsernameFieldWithText(_ text: String)
    func setImageOnTouchIDButton(imageName: String, for state: UIControlState)
    func hideTouchIDButton()
}

//
//  LoginViewController.swift
//  TouchID
//
//  Created by Tomasz Bilski on 11.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation
import UIKit

enum LoginButtonTag: Int {
    case create
    case login
}

class LoginViewController: UIViewController, LoginViewInterface {
    var eventHandler: LoginModuleInterface?
    
    let login = LoginButtonTag.login.rawValue
    let create = LoginButtonTag.create.rawValue
    
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var touchIDButton: UIButton!
    
    @IBAction func touchIDAction(_ sender: Any) {
        eventHandler?.startBioAuthentication()
    }
    
    @IBAction func loginButtonAction(_ sender: UIButton) {
        guard let newAccountName = usernameField.text,
            let newPassword = passwordField.text,
            !newAccountName.isEmpty,
            !newPassword.isEmpty else {
                eventHandler?.displayAlertViewForIncorrectCredentials()
                return
        }
        
        usernameField.resignFirstResponder()
        passwordField.resignFirstResponder()
        
        if sender.tag == create {
            eventHandler?.createCredentials(username: usernameField.text!, password: passwordField.text!)
        } else if sender.tag == login {
            eventHandler?.checkCredentials(username: usernameField.text!, password: passwordField.text!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventHandler?.adjustLoginViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        eventHandler?.startBioAuthentication()
    }
    
    func setLoginButtonTitle(text: String, for state: UIControlState) {
        loginButton.setTitle(text, for: .normal)
    }
    
    func setLoginButtonTag(_ tag: LoginButtonTag) {
        loginButton.tag = tag.rawValue
    }
    
    func setUsernameFieldWithText(_ text: String) {
        usernameField.text = text
    }
    
    func setImageOnTouchIDButton(imageName: String, for state: UIControlState) {
        touchIDButton.setImage(UIImage(named: imageName), for: state)
    }
    
    func hideTouchIDButton() {
        touchIDButton.isHidden = true
    }
}

//
//  LoginInteractor.swift
//  TouchID
//
//  Created by Tomasz Bilski on 12.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

protocol LoginInteractorInput {
    func checkIfPasswordIsCorrect(username: String, password: String) -> Bool
    func savePassword(username: String, password: String)
    func checkIfCredentialsExist() -> Bool
    func getUsername() -> String?
    func storeUsername(_ username: String)
    func canEvaluatePolicy() -> Bool
    func isFaceIDAvailable() -> Bool
    func setCredentialsExist()
    func startBioAuthentication()
    func logUserIn()
    func logUserOut()
}

protocol LoginInteractorOutput: AnyObject {
    func authenticationFailed(message: String)
    func authenticationSucceded()
}

//
//  LoginInteractorImpl.swift
//  TouchID
//
//  Created by Tomasz Bilski on 12.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

class LoginInteractorImpl: NSObject, LoginInteractorInput {
    weak var output: LoginInteractorOutput?
    let authenticationManager: AuthenticationManager
    
    init(authenticationManager: AuthenticationManager) {
        self.authenticationManager = authenticationManager
    }
    
    func checkIfCredentialsExist() -> Bool {
        return authenticationManager.checkIfCredentialsExist()
    }
    
    func getUsername() -> String? {
        return authenticationManager.getUsername()
    }
    
    func storeUsername(_ username: String) {
        authenticationManager.storeUsername(username)
    }
    
    func canEvaluatePolicy() -> Bool {
        return authenticationManager.canEvaluatePolicy()
    }
    
    func checkIfPasswordIsCorrect(username: String, password: String) -> Bool {
        return authenticationManager.checkIfPasswordIsCorrect(username: username,
                                                              password: password)
    }
    
    func savePassword(username: String, password: String) {
        authenticationManager.savePassword(username: username, password: password)
    }
    
    func setCredentialsExist() {
        authenticationManager.setCredentialsExist()
    }
    
    func startBioAuthentication() {
        /* tu tez można by było zrobic mapowanie (na wzor tego w ListInteractorImpl), tak zeby Interactor przekazywal jakiegos bundlea z message do Presentera */
        authenticationManager.authenticateUser { [weak self] message in
            if let message = message {
                self?.output?.authenticationFailed(message: message)
            } else {
                self?.output?.authenticationSucceded()
            }
        }
    }
    
    func logUserOut() {
        authenticationManager.logUserOut()
    }
    
    func logUserIn() {
        authenticationManager.logUserIn()
    }
    
    func isFaceIDAvailable() -> Bool {
        switch authenticationManager.biometricType() {
        case .faceID:
            return true
        default:
            return false
        }
    }
}

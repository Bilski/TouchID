//
//  LoginModuleDelegate.swift
//  TouchID
//
//  Created by Tomasz Bilski on 11.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

protocol LoginModuleDelegate: AnyObject {
    func loginModuleAuthenticationSucceeded()
}

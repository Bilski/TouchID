//
//  LoginModuleInterface.swift
//  TouchID
//
//  Created by Tomasz Bilski on 11.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

protocol LoginModuleInterface {
    func adjustLoginViews()
    func startBioAuthentication()
    func displayAlertViewForIncorrectCredentials()
    func createCredentials(username: String, password: String)
    func checkCredentials(username: String, password: String)
}

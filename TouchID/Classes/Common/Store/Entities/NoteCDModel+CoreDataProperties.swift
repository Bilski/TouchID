//
//  NoteCDModel+CoreDataProperties.swift
//  TouchID
//
//  Created by Tomasz Bilski on 09.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//
//

import Foundation
import CoreData


extension NoteCDModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NoteCDModel> {
        return NSFetchRequest<NoteCDModel>(entityName: "NoteCDModel")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var noteText: String?

}

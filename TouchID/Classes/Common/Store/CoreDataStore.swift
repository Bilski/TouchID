//
//  CoreDataStore.swift
//  TouchID
//
//  Created by Tomasz Bilski on 09.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStore: NSObject {
    var persistentContainer: NSPersistentContainer
    
    override init() {
        persistentContainer = NSPersistentContainer(name: "TouchID")
        persistentContainer.loadPersistentStores { (storeDescription, error) in
            if let error = error as NSError? {
                //Na produkcji nalezaloby to ladnie obsluzyc, nie uzywajac fatalError(_:file:line:)
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        super.init()
    }
    
    func newNote() -> NoteCDModel {
        let context = persistentContainer.viewContext
        return NoteCDModel(context: context)
    }
    
    func fetchEntries(sortDescriptors: [NSSortDescriptor],
                      completionBlock: (([NoteCDModel]) -> Void)!) {
        let fetchRequest = NSFetchRequest<NoteCDModel>(entityName: "NoteCDModel")
        fetchRequest.sortDescriptors = sortDescriptors
        
        do {
            let context = persistentContainer.viewContext
            let queryResults = try context.fetch(fetchRequest)
            completionBlock(queryResults)
        } catch {
            //Na produkcji nalezaloby to ladnie obsluzyc, nie uzywajac fatalError(_:file:line:)
            let error = error as NSError
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    }
    
    func save() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                //Na produkcji nalezaloby to ladnie obsluzyc, nie uzywajac fatalError(_:file:line:)
                let error = error as NSError
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
    }
}

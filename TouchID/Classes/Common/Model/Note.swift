//
//  Note.swift
//  TouchID
//
//  Created by Tomasz Bilski on 10.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation

struct Note {
    let date: Date
    let text: String
    
    init(date: Date, text: String) {
        self.date = date
        self.text = text
    }
}

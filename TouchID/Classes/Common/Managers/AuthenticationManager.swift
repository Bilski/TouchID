//
//  AuthenticationManager.swift
//  TouchID
//
//  Created by Tomasz Bilski on 11.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation
import LocalAuthentication

struct KeychainConfiguration {
    static let serviceName = "TouchID"
    static let accessGroup: String? = nil
}

enum BiometricType {
    case none
    case touchID
    case faceID
}

class AuthenticationManager {
    private var context: LAContext
    
    private let userDefaults: UserDefaults
    private var loginReason = "Logging in with Touch ID"
    private var isAuthenticated = false
    
    private var hasLoginKey = "hasLoginKey"
    private var usernameKey = "username"
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
        
        context = LAContext()
        //context.touchIDAuthenticationAllowableReuseDuration = 200
    }
    
    func checkIfLoggedIn() -> Bool {
        return isAuthenticated
    }
    
    func checkIfCredentialsExist() -> Bool {
        return userDefaults.bool(forKey: hasLoginKey)
    }
    
    func setCredentialsExist() {
        userDefaults.set(true, forKey: hasLoginKey)
    }
    
    func storeUsername(_ username: String) {
        userDefaults.setValue(username, forKey: usernameKey)
    }
    
    func getUsername() -> String? {
        return userDefaults.value(forKey: usernameKey) as? String
    }
    
    func logUserIn() {
        isAuthenticated = true
    }
    
    func logUserOut() {
        isAuthenticated = false
    }
    
    func checkIfPasswordIsCorrect(username: String, password: String) -> Bool {
        do {
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: username,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            let keychainPassword = try passwordItem.readPassword()
            return password == keychainPassword
        } catch {
            //Na produkcji nalezaloby to ladnie obsluzyc, nie uzywajac fatalError(_:file:line:)
            fatalError("Error reading password from keychain - \(error)")
        }
    }
    
    func savePassword(username: String, password: String) {
        do {
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: username,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            try passwordItem.savePassword(password)
        } catch {
            //Na produkcji nalezaloby to ladnie obsluzyc, nie uzywajac fatalError(_:file:line:)
            fatalError("Error reading password from keychain - \(error)")
        }
    }
    
    func biometricType() -> BiometricType {
        let _ = canEvaluatePolicy()
        switch context.biometryType {
        case .none:
            return .none
        case .touchID:
            return .touchID
        case .faceID:
            return .faceID
        }
    }
    
    func canEvaluatePolicy() -> Bool {
        return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
    
    func authenticateUser(completion: @escaping (String?) -> Void) {
        guard canEvaluatePolicy() else {
            completion("Touch ID not available")
            return
        }
        
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: loginReason) { (success, evaluateError) in
            DispatchQueue.main.async { //bo policy evaluation wykonywane jest na prywatnym wątku
                // autentykacja się powiodła
                if success {
                    completion(nil)
                } else {
                    let message: String
                    
                    switch evaluateError {
                    case LAError.authenticationFailed?:
                        message = "There was a problem verifying your identity."
                    case LAError.userCancel?:
                        message = "You pressed cancel."
                    case LAError.userFallback?:
                        message = "You pressed password."
                    case LAError.biometryNotAvailable?:
                        message = "Face ID/Touch ID is not available."
                    case LAError.biometryNotEnrolled?:
                        message = "Face ID/Touch ID is not set up."
                    case LAError.biometryLockout?:
                        message = "Face ID/Touch ID is locked."
                    default:
                        message = "Face ID/Touch ID may not be configured"
                    }
                    completion(message)
                }
                self.context = LAContext() // workaround
            }
        }
    }
}

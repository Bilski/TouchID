//
//  AppDependencies.swift
//  TouchID
//
//  Created by Tomasz Bilski on 08.04.2018.
//  Copyright © 2018 Tomasz Bilski. All rights reserved.
//

import Foundation
import UIKit

class AppDependencies {
    var listRouting = ListRouting()
    
    init() {
        configureDependencies()
    }
    
    func installRootViewControllerIntoWindow(window: UIWindow) {
        listRouting.presentListInterfaceFromWindow(window: window)
    }
    
    private func configureDependencies() {
        let authenticationManager = AuthenticationManager(userDefaults: UserDefaults.standard)
        let coreDataStore = CoreDataStore()
        let rootRouting = RootRouting()
        
        let listDataManager = ListDataManager(dataStore: coreDataStore)
        let listInteractor = ListInteractorImpl(dataManager: listDataManager, authenticationManager: authenticationManager)
        let listPresenter = ListPresenter()
        
        let loginRouting = LoginRouting()
        let loginInteractor = LoginInteractorImpl(authenticationManager: authenticationManager)
        let loginPresenter = LoginPresenter()
        
        listInteractor.output = listPresenter
        
        listPresenter.listInteractor = listInteractor
        listPresenter.listRouting = listRouting
        
        listRouting.rootRouting = rootRouting
        listRouting.loginRouting = loginRouting
        listRouting.listPresenter = listPresenter
        
        listDataManager.dataStore = coreDataStore
        
        loginInteractor.output = loginPresenter
        
        loginPresenter.loginInteractor = loginInteractor
        loginPresenter.loginRouting = loginRouting
        loginPresenter.loginModuleDelegate = listPresenter
        
        loginRouting.loginPresenter = loginPresenter
    }
}
